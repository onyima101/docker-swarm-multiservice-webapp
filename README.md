# This is a docker-swarm multiservice multinode web application. 

All images are in dockerhub. The script ensures that all neccessary underlay network are created and the multiple services are created. This applications consists of the following components to function effectively.

1, Voting-app, written in python. 

2, Redis, for in-memory, key-value data store.

3, Worker. Written in .net

4, Database (postgres) 

5, Result-app, written in Node.js 


Access the Voting app and vote using your nodes IP:80

Access the Result app to view the result using your node IP:5001
