#!/bin/bash

docker network create --driver overlay frontend
docker network create --driver overlay backend
docker service create --name vote -p 80:80 --replicas=2  --network frontend onyima101/votingapp_vote:v1
docker service create --name redis --network frontend redis:3.2
docker service create --name worker --network frontend --network backend onyima101/votingapp_worker:v1
docker service create --name db --mount type=volume,source=db-data,target=/var/lib/postgresql/data --network backend --env POSTGRES_HOST_AUTH_METHOD=trust postgres:9.4
docker service create --name result -p 5001:80 --network backend onyima101/votingapp_result:v1
